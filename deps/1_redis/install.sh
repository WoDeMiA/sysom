#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-redis
OSS_URL=https://sysom.oss-cn-beijing.aliyuncs.com/redis
REDIS_DL_URL=https://download.redis.io/releases
REDIS_DIR=redis-5.0.14
REDIS_PKG=redis-5.0.14.tar.gz

setup_redis() {
    rpm -q --quiet redis || yum install -y redis
    ###we need redis version >= 5.0.0, check redis version###
    redis_version=`yum list all | grep "^redis.x86_64" | awk '{print $2}' | awk -F"." '{print $1}'`
    echo ${redis_version}
    if [ $redis_version -lt 5 ]
    then
        rpm -q --quiet gcc || yum install -y gcc
        rpm -q --quiet make || yum install -y make
        echo "redis version in yum repo is less than 5.0.0, we will compile redis(5.0.14) and install it."
        if [ ! -e ${REDIS_PKG} ]
        then
            wget ${OSS_URL}/${REDIS_PKG} || wget ${REDIS_DL_URL}/${REDIS_PKG}
            if [ ! -e ${REDIS_PKG} ]
            then
                echo "download ${REDIS_PKG} fail"
                exit 1
            fi
        fi
        echo "now uncompress ${REDIS_PKG}, then compile and install it."
        tar -zxvf ${REDIS_PKG}
        pushd ${REDIS_DIR}
        make
        mkdir -p ${DEPS_HOME}/redis
        cp redis.conf ${DEPS_HOME}/redis/
        cp src/redis-server ${DEPS_HOME}/redis/
        if [ $? -ne 0 ]
        then
            echo "redis compile or install error, exit 1"
            exit 1
        fi
        popd
    fi
}

install_app() {
    setup_redis
}

install_app
