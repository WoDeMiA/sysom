#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-redis

init_conf() {
    cp ${SERVICE_NAME}.ini /etc/supervisord.d/
    ###change the install dir base on param $1###
    sed -i "s;/usr/local/sysom;${APP_HOME};g" /etc/supervisord.d/${SERVICE_NAME}.ini
}

init_app() {
    redis_version=`yum list all | grep "^redis.x86_64" | awk '{print $2}' | awk -F"." '{print $1}'`
    if [ $redis_version -lt 5 ]
    then
        # redis install from source, register to
        echo "install redis from source"
        init_conf
        supervisorctl update
    else
        echo "redis already installed from yum"
        systemctl daemon-reload
        systemctl enable redis.service
        # redis install from yum, just do nothing
    fi
}

init_app

# Start
bash -x $BaseDir/start.sh