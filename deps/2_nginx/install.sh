#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

setup_nginx() {
    rpm -q --quiet nginx || yum install -y nginx
    systemctl enable nginx
}

install_app() {
    setup_nginx
}

install_app
