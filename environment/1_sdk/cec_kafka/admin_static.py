# -*- coding: utf-8 -*- #
"""
Time                2022/9/26 21:13
Author:             mingfeng (SunnyQjm), zhangque (Wardenjohn)
Email               mfeng@linux.alibaba.com, ydzhang@linux.alibaba.com
File                admin_static.py
Description:        static call method
"""
import sys
from typing import Optional, List
from itertools import chain
from clogger import logger
from cec_base.exceptions import TopicNotExistsException, \
    TopicAlreadyExistsException, ConsumerGroupNotExistsException, \
    ConsumerGroupAlreadyExistsException
from cec_base.meta import TopicMeta, PartitionMeta, ConsumerGroupMeta, \
    ConsumerGroupMemberMeta
from cec_base.exceptions import CecException
from cec_base.url import CecUrl
from .utils import raise_if_not_ignore
from .consume_status_storage import ConsumeStatusStorage
from .common import StaticConst
from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka import Consumer as ConfluentKafkaConsumer
from confluent_kafka import Consumer, TopicPartition
import confluent_kafka
from confluent_kafka.error import KafkaException, KafkaError


####################################################################
# Static function implementation of the management interface
####################################################################
def static_create_topic(kafka_admin_client: AdminClient,
                            topic_name: str = "", num_partitions: int = 1,
                            replication_factor: int = 1,
                            ignore_exception: bool = False,
                            expire_time: int = 24 * 60 * 60 * 1000) -> bool:
    try:
        res = kafka_admin_client.create_topics(
            [NewTopic(topic_name, num_partitions, replication_factor)])
        res.get(topic_name).result()
    except KafkaException as ke:
        if ke.args[0].code() == KafkaError.TOPIC_ALREADY_EXISTS:
            return raise_if_not_ignore(ignore_exception,
                                        TopicAlreadyExistsException(
                                            f"Topic {topic_name} already "
                                            f"exists."
                                        ))
        else:
            return raise_if_not_ignore(ignore_exception, ke)
    except Exception as e:
        return raise_if_not_ignore(ignore_exception, e)
    return True

def static_del_topic(kafka_admin_client: AdminClient, topic_name: str,
                         ignore_exception: bool = False):
    """static method of deleting one topic
    
    this method of deleting one topic can be invoked by static method

    Args:
        kafka_admin_client (AdminClient): _description_
        topic_name (str): _description_
        ignore_exception (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """
    try:
        res = kafka_admin_client.delete_topics([topic_name])
        res.get(topic_name).result()
    except KafkaException as ke:
        if ke.args[0].code() == KafkaError.UNKNOWN_TOPIC_OR_PART:
            return raise_if_not_ignore(ignore_exception,
                                        TopicNotExistsException(
                                            f"Someone else is creating or deleting "
                                            f"this topic."
                                        ))
        else:
            return raise_if_not_ignore(ignore_exception, ke)
    except Exception as e:
        return raise_if_not_ignore(ignore_exception, e)
    return True

def static_is_topic_exist(kafka_admin_client: AdminClient,
                              topic_name: str) -> bool:
    class_meta = kafka_admin_client.list_topics(topic_name)
    return class_meta.topics.get(topic_name).error is None

def static_get_topic_list(kafka_admin_client: AdminClient) -> [TopicMeta]:
    class_meta = kafka_admin_client.list_topics()
    res = []
    for topic in class_meta.topics.values():
        new_topic = TopicMeta(topic.topic)
        new_topic.error = topic.error
        for p_key, p_value in topic.partitions.items():
            new_topic.partitions[p_key] = PartitionMeta(p_value.id)
        res.append(new_topic)
    return res

def static_create_consumer_group(kafka_admin_client: AdminClient,
                                     url: CecUrl, consumer_group_id: str,
                                     ignore_exception: bool = False
                                     ):

    if KafkaAdmin.static_is_consumer_group_exist(kafka_admin_client,
                                                    consumer_group_id):
        return raise_if_not_ignore(
            ignore_exception, ConsumerGroupAlreadyExistsException(
                f"Consumer group {consumer_group_id} already exists."))

    _kafka_consumer_client = Consumer({
        'bootstrap.servers': url.netloc,
        "request.timeout.ms": 600000,
        'group.id': consumer_group_id,
        **url.params,
    })
    try:
        _kafka_consumer_client.subscribe(['__consumer_offsets'])
        _kafka_consumer_client.poll(0.1)
    except Exception as e:
        return raise_if_not_ignore(
            ignore_exception, e)
    return True

def static_is_consumer_group_exist(
            kafka_admin_client: AdminClient,
            consumer_group_id: str) -> bool:
    return len(kafka_admin_client.list_groups(consumer_group_id)) > 0

def static_get_consumer_group_list(
            kafka_admin_client: AdminClient
    ) -> [ConsumerGroupMeta]:
    groups = kafka_admin_client.list_groups()
    res = []
    for group in groups:
        new_group = ConsumerGroupMeta(group.id)
        new_group.error = group.error
        for member in group.members:
            new_group.members.append(
                ConsumerGroupMemberMeta(member.client_id))
        res.append(new_group)
    return res

