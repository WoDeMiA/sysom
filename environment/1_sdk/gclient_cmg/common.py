# -*- coding: utf-8 -*- #
"""
Time                2022/7/29 13:33
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""


class StaticConst:
    """Static consts

    This class defines all the static constant values in the cmg-redis module
    """
    # List of specialization parameters
    SPECIAL_PARM_CMG_PROTOCOL = \
        "cmg_protocol"
    SPECIAL_PARM_CMG_SERVICE_NAME = \
        "cmg_service_name"
    SPECIAL_PARM_CMG_LOAD_BALANCE_STRATEGY = \
        "cmg_load_balance_strategy"
    SPECIAL_PARM_CMG_FETCH_INTERVAL = \
        "cmg_fetch_interval"

    _special_parameter_list = [
        SPECIAL_PARM_CMG_PROTOCOL,
        SPECIAL_PARM_CMG_SERVICE_NAME,
        SPECIAL_PARM_CMG_LOAD_BALANCE_STRATEGY,
        SPECIAL_PARM_CMG_FETCH_INTERVAL
    ]

    _special_parameters_default_value = {
        SPECIAL_PARM_CMG_PROTOCOL: (str, "redis"),
        SPECIAL_PARM_CMG_SERVICE_NAME: (str, None),
        SPECIAL_PARM_CMG_LOAD_BALANCE_STRATEGY: (str, "RANDOM"),
        SPECIAL_PARM_CMG_FETCH_INTERVAL: (int, 5)
    }

    @classmethod
    def parse_special_parameter(cls, params: dict) -> dict:
        """Parse specialization parameters

        Parse the specialization parameters and remove the specialization
        parameters from the parameter list

        Args:
            params(dict): CecUrl.params

        Returns:

        """
        res = {}
        for key in cls._special_parameter_list:
            _type, default = \
                cls._special_parameters_default_value[key]
            res[key] = _type(params.pop(key, default))
        return res
