# -*- coding: utf-8 -*- #
"""
Time                2023/5/5 11:11
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                result.py
Description:
"""
from typing import List, Tuple, Dict, Union


class RangeVectorResult:
    def __init__(self, metric_name: str, labels: Dict[str, str],
                 values: List[Tuple[float, float]]):
        self.metric_name = metric_name
        self.labels = labels
        self.values = values

    def keys(self):
        return ('metric_name', 'labels', 'values')

    def __getitem__(self, item):
        return getattr(self, item)

    def to_dict(self):
        return dict(self)

class InstantVectorResult:
    def __init__(self, metric_name: str, labels: Dict[str, str],
                 value: Tuple[float, float]):
        self.metric_name = metric_name
        self.labels = labels
        self.value = value

    def keys(self):
        return ('metric_name', 'labels', 'value')

    def __getitem__(self, item):
        return getattr(self, item)

    def to_dict(self):
        return dict(self)


class MetricResult:
    def __init__(self, code: int,
                 data: Union[List[RangeVectorResult], List[InstantVectorResult], List[str]],
                 err_msg: str = ""):
        self.code = code
        self.err_msg = err_msg
        self.data = data

    def keys(self):
        return ("code", "err_msg", "data")

    def __getitem__(self, item):
        obj = getattr(self, item)
        if item == "data":
            if isinstance(obj, List) and len(obj) > 0:
                if isinstance(obj[0], RangeVectorResult):
                    return [item.to_dict() for item in obj]
                if isinstance(obj[0], InstantVectorResult):
                    return [item.to_dict() for item in obj]
            else:
                return obj
        else:
            return obj

    def to_dict(self):
        return dict(self)
