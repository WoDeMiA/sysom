# -*- coding: utf-8 -*- #
"""
Time                2023/4/28 14:55
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                exceptions.py
Description:
"""


class GCacheException(Exception):
    """GCache base exception

    This class defines the base exception for GCache, and all exceptions thrown
    by GCache should inherit from this class.
    """


class GCacheProtoAlreadyExistsException(GCacheException):
    """Exceptions thrown for duplicate proto(submodule) registration

    This exception should be thrown if the proto(submodule) already exists while
    registering a submodule.
    """


class GCacheProtoNotExistsException(GCacheException):
    """Exceptions thrown for trying to use a non-existent proto(submodule)

    Exceptions that will be thrown when trying to use a non-existent proto
    (submodule).
    """


class GCacheNotValidGCacheUrlException(GCacheException):
    """Exception thrown when an invalid GCacheUrl format is parsed."""
