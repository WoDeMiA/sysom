# -*- coding: utf-8 -*- #
"""
Time                2023/5/9 10:28
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                http_gclient.py
Description:
"""
import requests
from urllib.parse import urljoin
from gclient_base import GClient, GClientUrl
from .common import StaticConst


class HttpGClient(GClient):
    def __init__(self, url: GClientUrl):
        special_params = StaticConst.parse_special_parameter(url.params)
        tls = special_params.get("tls", False)
        protocol = "https" if tls else "http"
        self._base_url = f"{protocol}://{url.netloc}{url.path}"

    def get(self, path: str, params=None, **kwargs) -> requests.Response:
        return requests.get(
            urljoin(self._base_url, path), params, **kwargs
        )

    def options(self, path: str, **kwargs) -> requests.Response:
        return requests.options(
            urljoin(self._base_url, path), **kwargs
        )

    def head(self, path: str, **kwargs) -> requests.Response:
        return requests.head(
            urljoin(self._base_url, path), **kwargs
        )

    def post(self, path: str, data=None, json=None,
             **kwargs) -> requests.Response:
        return requests.post(
            urljoin(self._base_url, path), data, json, **kwargs
        )

    def put(self, path: str, data=None, **kwargs) -> requests.Response:
        return requests.put(
            urljoin(self._base_url, path), data, **kwargs
        )

    def patch(self, path: str, data=None, **kwargs) -> requests.Response:
        return requests.patch(
            urljoin(self._base_url, path), data, **kwargs
        )

    def delete(self, path: str, **kwargs) -> requests.Response:
        return requests.delete(
            urljoin(self._base_url, path), **kwargs
        )
