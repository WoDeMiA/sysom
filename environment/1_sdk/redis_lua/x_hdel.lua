for i = 1, #ARGV do
    local field = ARGV[i]
    -- 删除键值
    redis.call("HDEL", KEYS[1], field)
    -- 删除过期时间
    redis.call("HDEL", KEYS[2], field)
end
return "OK"