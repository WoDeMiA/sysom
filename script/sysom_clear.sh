#!/bin/bash -x
set -x
ProgName=$(basename $0)
BaseDir=$(dirname $(readlink -f "$0"))
SYSOM_CLEAR_LOG=$LOG_HOME/sysom_clear.log

####################################################################################################################
# Helper functions
# sysom_clear.sh is used to clear the server but not uninstall, it can be reverted by using sysom_init.sh
####################################################################################################################

yellow() {
    printf '\33[1;33m%b\n\33[0m' "$1"
}

red() {
    printf '\33[1;31m%b\n\33[0m' "$1"
}

green() {
    printf '\33[1;32m%b\n\33[0m' "$1"
}

do_clear_microservice() {
    target=$1
    targets=(${target//,/ })
    for target in ${targets[*]}; do
        for service in $(ls ${MICROSERVICE_HOME} | grep ${target}); do
            target_script_dir=${MICROSERVICE_SCRIPT_HOME}/${service}
            if [ ! -d "$target_script_dir" ] || [ ! -f "${target_script_dir}/clear.sh" ]; then
                continue
            fi

            pushd ${target_script_dir}

            green "$target_script_dir clear..................................."
            bash -x ${target_script_dir}/clear.sh
            if [ $? -ne 0 ]; then
                red "$target_script_dir clear fail, please check..................................."
                red "sysom clear failed, exit 1"
                exit 1
            fi
            popd
        done
    done
}

do_clear_environment() {
    target=$1
    targets=(${target//,/ })
    for target in ${targets[*]}; do
        for env in $(ls ${ENVIRONMENT_HOME} | grep ${target}); do
            target_dir=${ENVIRONMENT_HOME}/${env}
            if [ ! -d "${target_dir}" ] || [ ! -f "${target_dir}/clear.sh" ]; then
                continue
            fi

            pushd ${target_dir}

            green "$target_dir Clear..................................."
            bash -x ${target_dir}/clear.sh
            if [ $? -ne 0 ]; then
                red "$target_dir clear fail, please check..................................."
                red "sysom clear failed, exit 1"
                exit 1
            fi

            popd
        done
    done
}

do_clear_deps() {
    target=$1
    targets=(${target//,/ })
    for target in ${targets[*]}; do
        for dep in $(ls ${DEPS_HOME} | grep ${target}); do
            target_dir=${DEPS_HOME}/${dep}
            if [ ! -d "${target_dir}" ] || [ ! -f "${target_dir}/clear.sh" ]; then
                continue
            fi

            pushd ${target_dir}

            green "$target_dir Clear..................................."
            bash -x ${target_dir}/clear.sh
            if [ $? -ne 0 ]; then
                red "$target_dir clear fail, please check..................................."
                red "sysom clear failed, exit 1"
                exit 1
            fi
            popd
        done

    done
}

####################################################################################################################
# Subcommands
####################################################################################################################

sub_help() {
    echo "Usage: $ProgName <subcommand> [options]"
    echo "Subcommands:"
    echo "    all                                           Clear all modules"
    echo "    env   [ALL | <base_env_name>]        Clear all enviroment or specific enviroment"
    echo "          Example: $ProgName env env"
    echo "          Example: $ProgName env sdk"
    echo "    deps  [ALL | <deps_name>]            Clear all deps or specific dep"
    echo "          Example: $ProgName dep mysql"
    echo "          Example: $ProgName dep grafana"
    echo "    ms    [ALL | <service_name>]         Clear all microservices or specific microservice"
    echo "          Example: $ProgName ms sysom_diagnosis"
    echo ""
    echo "For help with each subcommand run:"
    echo "$ProgName <subcommand> -h|--help"
    echo ""
}

sub_ALL() {
    sub_ms ALL
    sub_env ALL
    sub_deps ALL
}

sub_all() {
    sub_ALL
}

sub_env() {
    sub_environment $@
}

# -> env + sdk
sub_environment() {
    target=$1
    if [ "$target" == "ALL" ]; then
        # Clear all microservices
        for env in $(ls -r $ENVIRONMENT_HOME); do
            if [ ! -d "${ENVIRONMENT_HOME}/${env}" ]; then
                continue
            fi
            do_clear_environment $env
        done
    else
        # Clear specific microservices
        do_clear_environment $target
    fi
}

sub_ms() {
    sub_microservice $@
}

# All microservices
sub_microservice() {
    target=$1
    if [ "$target" == "ALL" ]; then
        # Clear all microservices
        for microservice in $(ls -r $MICROSERVICE_HOME); do
            if [ ! -d "${MICROSERVICE_HOME}/${microservice}" ]; then
                continue
            fi
            do_clear_microservice ${microservice}
        done
    else
        # CLear specific microservices
        do_clear_microservice $target
    fi
}

sub_deps() {
    target=$1
    if [ "$target" == "ALL" ]; then
        # Clear all deps
        for dep in $(ls -r $DEPS_HOME); do
            if [ ! -d "${DEPS_HOME}/${dep}" ]; then
                continue
            fi
            do_clear_deps ${dep}
        done
    else
        # Clear specific dep
        do_clear_deps $target
    fi
}

# Clear web
sub_web() {
    yellow "all sysom servers depend on the ${subcommand}, it can't be clear."
}

subcommand=$1
case $subcommand in
"" | "-h" | "--help")
    sub_help
    ;;
*)
    shift
    sub_${subcommand} $@ | tee ${SYSOM_CLEAR_LOG}
    if [ $? = 127 ]; then
        echo "Error: '$subcommand' is not a known subcommand." >&2
        echo "       Run '$ProgName --help' for a list of known subcommands." >&2
        exit 1
    fi
    ;;
esac
