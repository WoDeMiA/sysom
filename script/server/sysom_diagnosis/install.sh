#!/bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
SERVICE_SCRIPT_HOME=${MICROSERVICE_SCRIPT_HOME}/${SERVICE_SCRIPT_DIR}
VIRTUALENV_HOME=$GLOBAL_VIRTUALENV_HOME
DOWNLOAD_URL=https://sysom-publish.oss-cn-shenzhen.aliyuncs.com/sysom_deps
STANDALONE_PACKAGE=standalone-1.2.0-SNAPSHOT.jar
SERVICE_NAME=sysom-diagnosis


if [ "$UID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

install_requirement() {
    pushd ${SERVICE_SCRIPT_HOME}
    pip install -r requirements.txt
    popd
}

source_virtualenv() {
    echo "INFO: activate virtualenv..."
    source ${VIRTUALENV_HOME}/bin/activate || exit 1
}

prepare_node() {
    source_virtualenv
    pushd ${SERVICE_SCRIPT_HOME}
    python prepare_node.py /etc/sysom/config.yml ${SERVICE_HOME}/config.yml x86_64 || exit 1
    popd
}

install_required_packages() {
    # required jdk
    # Check whether java command exists, not exists then install
    if ! command -v java >/dev/null 2>&1; then
        echo "INFO: install java..."
        rpm -q --quiet java-1.8.0-openjdk || yum install -y java-1.8.0-openjdk
    fi

    prepare_node

    pushd ${SERVICE_HOME}/service_scripts
    mkdir -p jfrFlold
    if [ ! -f jfrFlold/${STANDALONE_PACKAGE} ]; then
        echo "INFO: download ${STANDALONE_PACKAGE}..."
        wget ${DOWNLOAD_URL}/${STANDALONE_PACKAGE} -O jfrFlold/${STANDALONE_PACKAGE}
    fi
    popd
}

install_app() {
    source_virtualenv
    install_requirement
    install_required_packages
}

install_app
