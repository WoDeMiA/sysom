import sys
from sysom_utils import NodeManager, ConfigParser

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python node_file_prepare.py <global_config_path> <service_config_path> <arch>")
        sys.exit(1)
    global_config_path = sys.argv[1]
    service_config_path = sys.argv[2]
    arch = sys.argv[3]
    config = ConfigParser(global_config_path, service_config_path)
    NodeManager(config, None).prepare_files(arch)