#!/bin/sh
yum install -y git docker

sudo docker pull registry.cn-hangzhou.aliyuncs.com/sysom/sysom:v7.0
mkdir -p /tmp/sysak
sudo docker run -d -v /tmp/sysak:/home/sysak -it --name "sysak_rpm_build" registry.cn-hangzhou.aliyuncs.com/sysom/sysom:v7.0 /bin/bash
#sudo docker exec -it sysak_rpm_build sh -c "cd /home/;git clone -b v2.1.0-rc1 https://gitee.com/anolis/sysak.git"
sudo docker exec -it sysak_rpm_build sh -c "cd /home/;git clone -b v$1 https://gitee.com/anolis/sysak.git"
sudo docker exec -it sysak_rpm_build sh -c "cd /home/sysak/rpm/;./sysak-build-nodep.sh $1 1"
sudo docker exec -it sysak_rpm_build sh -c "cp /home/sysak/rpm/BUILDROOT/sysak-$1-1.x86_64 /home/sysak/"
echo "sysak-$1-1.x86_64.rpm in /tmp/sysak/"
