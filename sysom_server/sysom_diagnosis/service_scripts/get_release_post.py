"""
Time                2023/07/21 10:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                get_release_post.py
Description:
"""
from typing import List
import json
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult


class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        postprocess_result = PostProcessResult(
            code=0,
            err_msg="",
            result={}
        )
        with open(results[0].file_list[0].local_path, "r") as f:
            release_info = f.read()
        postprocess_result.result = {
            "KernelVersion": {"data": [{"key": "", "value": results[0].stdout}]},
            "ReleaseInfo": {"data": [{"key": "", "value": release_info}]}
        }
        print(json.dumps(postprocess_result.result, indent=4))
        return postprocess_result