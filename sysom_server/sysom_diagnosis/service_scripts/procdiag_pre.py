import re
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask, FileItem
class PreProcessor(DiagnosisPreProcessor):
    """Get release info diagnosis
    Just invoke command in target instance and get stdout result
    Args:
        DiagnosisPreProcessor (_type_): _description_
    """
    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        # 从前端传递的参数中读取目标实例IP
        instance = params.get("instance", "")
        time = params.get("time", "")
        ipport = params.get("ipport", "")
        resp = is_valid_ip_port(ipport)
        if not resp:
            raise Exception("IP: PORT is empty or format does not meet the requirements")
        time = int(time) * 10
        cmd = "sysak rtrace --tcpping --dst {} -c {} --period 0.1 --iqr".format(ipport, time)
        return DiagnosisTask(
            jobs=[DiagnosisJob(instance=instance, cmd=cmd)],
            in_order = False,
        )
        

def is_valid_ip_port(ip_port):
    pattern = r'^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):(\d{1,5})$'
    match = re.match(pattern, ip_port)
    if match:
        ip = match.group(1)
        port = int(match.group(2))
        if 0 <= port <= 65535:
            return True
    return False
