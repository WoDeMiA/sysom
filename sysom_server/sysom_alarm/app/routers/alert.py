# -*- coding: utf-8 -*- #
"""
Time                2023/08/28 19:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                health.py
Description:
"""
from typing import List
from clogger import logger
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.crud import get_alert_datas, change_alert_deal_status, change_alert_deal_status_by_alert_id
from app.database import get_db
from app.schemas import AlertDataFull, MarkAsReadParams
from app.query import AlertDataQueryParams
from sysom_utils import StandardListResponse, StandardResponse


router = APIRouter()


@router.put("/mark_as_read")
async def mark_alert_data_as_read(
    params: MarkAsReadParams, db: Session = Depends(get_db)
):
    if params.ids is not None and len(params.ids) > 0:
        res = change_alert_deal_status(db, params.ids, 1)
    elif params.alert_ids is not None:
        res = change_alert_deal_status_by_alert_id(db, params.alert_ids, 1)
    else:
        return StandardResponse.error("ids or alert_ids must be specified")
    if res is None:
        return StandardResponse.error(f"AlertData not exists with id in {params.ids}")
    return StandardResponse.success(None)


@router.get("/list")
async def get_alert_list(
    query_params: AlertDataQueryParams = Depends(), db: Session = Depends(get_db)
):
    alert_data_list, total_count = get_alert_datas(db, query_params)

    return StandardListResponse(alert_data_list, AlertDataFull, total=total_count)
