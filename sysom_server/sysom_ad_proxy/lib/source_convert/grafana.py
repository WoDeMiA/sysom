import uuid
from typing import List
from app.schemas import AlertType, AlertStatus, AlertData
from .base import SourceConverterBase


class SourceConverter(SourceConverterBase):
    def convert(self, alert_source_type: str, alert_data: dict) -> List[AlertData]:
        """Convert grafana alert data to SysOM AlertData
        {
            "receiver":"sysom-alarm-center",
            "status":"firing",
            "alerts":[
                {
                    "status":"firing",
                    "labels":{
                        "alertname":"\u6d4b\u8bd5\u89c4\u5219",
                        "grafana_folder":"rules",
                        "label111":"label_value"
                    },
                    "annotations":{
                        "description":"test description",
                        "summary":"test summary"
                    },
                    "startsAt":"2023-08-28T16:30:00+08:00",
                    "endsAt":"0001-01-01T00:00:00Z",
                    "generatorURL":"http://localhost/grafana/alerting/grafana/Xr445RkSz/view",
                    "fingerprint":"45d174bfd670559f",
                    "silenceURL":"http://localhost/grafana/alerting/silence/new?alertmanager=grafana&matcher=alertname%3D%E6%B5%8B%E8%AF%95%E8%A7%84%E5%88%99&matcher=grafana_folder%3Drules&matcher=label111%3Dlabel_value",
                    "dashboardURL":"",
                    "panelURL":"",
                    "valueString":"[ var='B0' metric='Value' labels={__name__=sysak_proc_cpu_total, exported_instance=192.168.0.121, instance=127.0.0.1:8405, job=auto_discovery, mode=idle, source=sysom_monitor_server} value=99.6 ], [ var='B1' metric='Value' labels={__name__=sysak_proc_cpu_total, exported_instance=192.168.0.251, instance=192.168.0.251:8405, job=auto_discovery, mode=idle, source=sysom_monitor_server} value=99.1 ]"
                }
            ],
            "groupLabels":{
                "alertname":"\u6d4b\u8bd5\u89c4\u5219",
                "grafana_folder":"rules"
            },
            "commonLabels":{
                "alertname":"\u6d4b\u8bd5\u89c4\u5219",
                "grafana_folder":"rules",
                "label111":"label_value"
            },
            "commonAnnotations":{
                "description":"test description",
                "summary":"test summary"
            },
            "externalURL":"http://localhost/grafana/",
            "version":"1",
            "groupKey":"{}:{alertname=\"\u6d4b\u8bd5\u89c4\u5219\", grafana_folder=\"rules\"}",
            "truncatedAlerts":0,
            "orgId":1,
            "title":"[FIRING:1] \u6d4b\u8bd5\u89c4\u5219 rules (label_value)",
            "state":"alerting",
            "message":"**Firing**\n\nValue: [ var='B0' metric='Value' labels={__name__=sysak_proc_cpu_total, exported_instance=192.168.0.121, instance=127.0.0.1:8405, job=auto_discovery, mode=idle, source=sysom_monitor_server} value=99.6 ], [ var='B1' metric='Value' labels={__name__=sysak_proc_cpu_total, exported_instance=192.168.0.251, instance=192.168.0.251:8405, job=auto_discovery, mode=idle, source=sysom_monitor_server} value=99.1 ]\nLabels:\n - alertname = \u6d4b\u8bd5\u89c4\u5219\n - grafana_folder = rules\n - label111 = label_value\nAnnotations:\n - description = test description\n - summary = test summary\nSource: http://localhost/grafana/alerting/grafana/Xr445RkSz/view\nSilence: http://localhost/grafana/alerting/silence/new?alertmanager=grafana&matcher=alertname%3D%E6%B5%8B%E8%AF%95%E8%A7%84%E5%88%99&matcher=grafana_folder%3Drules&matcher=label111%3Dlabel_value\n"
        }

        Args:
            alert_data (dict): Grafana grafana data


        Returns:
            AlertData: _description_
        """
        alerts = alert_data.get("alerts", [])
        res = []

        for alert in alerts:
            item = AlertData(
                alert_id=str(uuid.uuid4()),
                instance="",
                alert_item=alert.get("labels", {}).get("alertname", ""),
                alert_category=AlertType.MONITOR,
                alert_source_type=alert_source_type,
                alert_time=SourceConverterBase.iso_to_timestamp(
                    alert.get("startsAt", "")
                ),
                status=alert.get("status", AlertStatus.FIRING).upper(),
                labels=alert.get("labels", {}),
                annotations=alert.get("annotations", {}),
                origin_alert_data=alert,
            )
            res.append(item)
        return res
