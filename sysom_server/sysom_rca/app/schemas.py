# -*- coding: utf-8 -*- #
"""
Time                2023/08/02 14:32
Author:             shiyan
Email               chenshiyan@linux.alibaba.com
File                schemas.py
Description:
"""
from pydantic import BaseModel

###########################################################################
# Define schemas here
###########################################################################

class RcaCallRecord(BaseModel):
    recordid: str
    state: str
    url: str
    user: str
    timestamp: int
    
    class Config:
        orm_mode = True

class RcaItemsRecord(BaseModel):
    recordid: str
    machine_ip: str
    state: str
    url: str
    user: str
    time_occur: int
    time_start: int
    time_end: int
    metric_dict: str
    rca_conclusion: str
    final_conclusion: str

    class Config:
        orm_mode = True
