import numpy as np
import pandas as pd

from .utils import ts_2_hour
from .algo_stat import Stat
import math
from clogger import logger


class Algorithm:
    def __init__(self) -> None:
        pass

    def learn(self, ts: float, util: float) -> None:
        pass

    def predict(self, ts: float) -> float:
        pass

    def report(self) -> None:
        pass


class PeriodAlgo(Algorithm):
    def __init__(self, window_minutes: int, interval: int, half_life: int) -> None:
        self._window_minutes = window_minutes
        self._interval = interval
        self._nr_window = int((24 * 60) / window_minutes)
        self._slots = int(100 / interval)
        self._model = np.zeros(self._nr_window * self._slots).reshape(
            self._nr_window, self._slots
        )

        self._decay_factor = math.pow(1 / 2, 1 / half_life)
        # self._table = np.logspace(
        #     start=0, stop=half_life, endpoint=True, base=self._decay_factor
        # )
        self._cur_window = -1
        self._reset()
        self._train_times = 0
        self._slot_stats = [Stat() for i in range(self._nr_window)]
        self._stat = Stat()
        self._ts = 0
        self._history = [[] for i in range(self._nr_window)]
        logger.info(
            f"window_minutes={self._window_minutes} interval={self._interval} window={self._nr_window} slots={self._slots} decay_coefficient={self._decay_factor}"
        )

    def _reset(self) -> None:
        self._buffer = []

    def _update(self, window: int, util: float, ts: float) -> None:
        self._buffer.append(util)
        self._cur_window = window
        self._ts = ts if ts > self._ts else self._ts

    def _quantile(self):
        return pd.Series(self._buffer).quantile(0.95)

    def _avg(self):
        return pd.Series(self._buffer).mean()

    def _max(self):
        return pd.Series(self._buffer).max()

    def _record_window_avg(self) -> None:
        if self._cur_window == -1:
            return
        util = self._quantile()
        real = int(util / self._interval)
        self._history[self._cur_window].append(util)
        self._model[self._cur_window] *= self._decay_factor
        self._model[self._cur_window][real] += 1
        self._train_times += 1

    def _record(self, window, expect, real) -> None:
        self._stat._record(expect, real)
        self._slot_stats[window]._record(expect, real)

    def train(self, arr: np.ndarray) -> None:
        if arr.shape[0] != self._nr_window:
            print("Invalid train data.")
            return

        for window, series in enumerate(arr):
            for util in series:
                self.learn(window, util, 1)

    def predict(self, ts: float) -> float:
        window = ts_2_hour(ts)
        expect = self._model.argmax(axis=1)[window]
        result = expect * self._interval
        logger.debug(f"model predict ts={ts} window={window} result={result}")
        return result

    def learn(self, ts: float, util: float) -> None:
        window = ts_2_hour(ts)
        if ts < self._ts:
            logger.error("occur coming data is older than model.")
            return
        if self._cur_window != window:
            self._record_window_avg()
            self._reset()
        self._update(window, util, ts)

    def report(self) -> None:
        for i in range(self._nr_window):
            logger.debug(
                f"predict window={i} util={self.predict(self._window_minutes * i * 60 + 16*60*60):.1f}"
            )
        # self._stat.report()

    def predict_future(self):
        future_watermark = self._model.argmax(axis=1) * self._interval
        return future_watermark


class RtAlgo(Algorithm):
    def __init__(self, interval, half_life: int) -> None:
        self._interval = interval
        self._decay_factor = math.pow(1 / 2, 1 / half_life)
        self._model = np.zeros(int(100.0 / self._interval))
        self._stat = Stat()

    def learn(self, ts: float, util: float) -> None:
        index = int(util / self._interval)
        self._model *= self._decay_factor
        self._model[index] += 1

    def predict(self, ts: float) -> float:
        expect = self._model.argmax() * self._interval
        return expect

    def _record(self, expect, real) -> None:
        self._stat._record(expect, real)

    def report(self) -> None:
        self._stat.report()


class MixAlgo:
    def __init__(self) -> None:
        self.rt_algo = RtAlgo(0.1, 4)
        self.period_algo = PeriodAlgo(60, 0.1, 4096)
        self._stat = Stat()

    def learn(self, window: int, util: float):
        self.rt_algo.learn(util)
        self.period_algo.learn(window, util)

    def predict(self, window: int, util: float) -> float:
        rt_result = self.rt_algo.predict()
        period_result = self.period_algo.predict(window)
        if util is not None:
            self.rt_algo._record(rt_result, util)
            self.period_algo._record(window, period_result, util)

        factor = self.period_algo.slot_stats[window].avg_error() / (
            self.rt_algo.stat.avg_error()
            + self.period_algo.slot_stats[window].avg_error()
        )
        factor = 0
        mix_result = factor * rt_result + (1 - factor) * period_result

        print(
            "rt={:.2f} period={:.2f} mix={:.2f} real={:.2f}".format(
                rt_result, period_result, mix_result, util
            )
        )
        return mix_result

    def _record(self, expect, real):
        self._stat._record(expect, real)

    def report(self):
        print("mix:")
        self._stat.report()
        print("rt:")
        self.rt_algo.report()
        print("period:")
        self.period_algo.report()
