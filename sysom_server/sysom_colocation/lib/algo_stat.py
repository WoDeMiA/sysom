
class Stat:
    def __init__(self) -> None:
        self.all = 0
        self.succ = 0
        self.error = 0

    def record(self, expect: float, real: float) -> None:
        self.all += 1
        error = abs(expect - real)
        if error != 0:
            self.error += error
        else:
            self.succ += 1

    def report(self) -> None:
        if self.all > 0:
            print(
                "predict={} success={} rate={:.2f} error_avg={:.2f}".format(
                    self.all,
                    self.succ,
                    float(self.succ) / self.all,
                    float(self.error) / self.all,
                )
            )

    def avg_error(self) -> float:
        if self.all > 0:
            return float(self.error) / self.all
        else:
            return 0
