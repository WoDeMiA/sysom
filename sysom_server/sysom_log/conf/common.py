# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""
from pathlib import Path
from sysom_utils import ConfigParser, SysomFramework, CecTarget

BASE_DIR = Path(__file__).resolve().parent.parent

##################################################################
# Load yaml config first
##################################################################
YAML_GLOBAL_CONFIG_PATH = f"{BASE_DIR.parent.parent}/conf/config.yml"
YAML_SERVICE_CONFIG_PATH = f"{BASE_DIR}/config.yml"

YAML_CONFIG = ConfigParser(YAML_GLOBAL_CONFIG_PATH, YAML_SERVICE_CONFIG_PATH)

mysql_config = YAML_CONFIG.get_server_config().db.mysql
service_config = YAML_CONFIG.get_service_config()

SysomFramework.init(YAML_CONFIG)

##################################################################
# fastapi config
##################################################################
SQLALCHEMY_DATABASE_URL = (
    f"{mysql_config.dialect}+{mysql_config.engine}://{mysql_config.user}:{mysql_config.password}@"
    f"{mysql_config.host}:{mysql_config.port}/{mysql_config.database}"
)
# ASYNC_SQLALCHEMY_DATABASE_URL = (
#     f"{mysql_config.dialect}+aiomysql://{mysql_config.user}:{mysql_config.password}@"
#     f"{mysql_config.host}:{mysql_config.port}/{mysql_config.database}"
# )

##################################################################
# Cec settings
##################################################################
SYSOM_CEC_URL = YAML_CONFIG.get_cec_url(CecTarget.PRODUCER)
SYSOM_CEC_LOG_DISPATCH_NODE_EVENT_TOPIC = "node_event"
SYSOM_CEC_LOG_DISPATCH_NODE_EVENT_GROUP = "SYSOM_NODE_LOG_EVENT_GROUP"
SYSOM_CEC_LOG_DISPATCH_AUDIT_EVENT_TOPIC = "AUDIT_EVENT"
SYSOM_CEC_LOG_DISPATCH_AUDIT_EVENT_GROUP = "SYSOM_AUDIT_LOG_EVENT_GROUP"
