CALCULATE_INTERVAL = 30

# where to store the abnormal metrics list
# mysql: store in mysql database
# prometheus: expose as prometheus metrics
ABNORMAL_METRIC_STORAGE = "mysql"

# Health Algorithm for calculating the specific level
# default: default algorithm
# weightedSum: weighted sum algorithm
CLUSTER_ALGORITHM = "default"
NODE_ALGORITHM = "default"
POD_ALGORITHM = "default"


#################################################################################
# Cluster Health Metrics
#################################################################################

CLUSTER_HEALTH_METRICS = []

#################################################################################
# Node Health Metrics
#################################################################################

NODE_HEALTH_METRICS = [
    ##########################################
    # Node Saturation Metrics   
    #########################################
    {
        "MetricID": "Node file descriptor util",
        "Type": "capacity",
        "Weight": 0.2,
    },
    {
        "MetricID": "Node memory util",
        "Type": "capacity",
        "Weight": 0.1,
    },
    {
        "MetricID": "Node cpu util",
        "Type": "capacity",
        "Weight": 0.1,
    },
    {
        "MetricID": "Node sys util",
        "Type": "capacity",
        "Weight": 0.3,   
    },
    {
        "MetricID": "Node rootfs util",
        "Type": "capacity",
        "Weight": 0.1,
    },
    {
        "MetricID": "Node rootfs inode util",
        "Type": "capacity",
        "Weight": 0.2,
    },
    #########################################
    # Node load Metrics   
    #########################################
    {
        "MetricID": "Node load average",
        "Type": "load",
        "Weight": 1.0,      
    },
    #########################################
    # Node latency Metrics   
    #########################################
    {
        "MetricID": "Node sched latency",
        "Type": "latency",
        "Weight": 1.0,       
    },
    #########################################
    # Node Error Metrics   
    #########################################
    {
        "MetricID": "Node OOM count",
        "Type": "error",
        "Weight": 1.0,  
    },
]

#################################################################################
# Pod Health Metrics
#################################################################################

POD_HEALTH_METRICS = [
    #########################################
    # Pod Capacity Metrics   
    #########################################
    {
        "MetricID": "Pod memory util",
        "Type": "capacity",
        "Weight": 0.3,
    },
    {
        "MetricID": "Pod cpu util",
        "Type": "capacity",
        "Weight": 0.2,
    },
    {
        "MetricID": "Pod sys util",
        "Type": "capacity",
        "Weight": 0.5,
    },
    #{
    #    "MetricID": "Pod rootfs util",
    #    "Type": "capacity",
    #    "Weight": 0.1,
    #},
    #{
    #    "MetricID": "Pod rootfs inode util",
    #    "Type": "capacity",
    #    "Weight": 0.1,
    #},
    #########################################
    # Pod Load Metrics   
    #########################################
    {
        "MetricID": "Pod load average",
        "Type": "load",
        "Weight": 1.0,
    },
    #########################################
    # Pod Latency Metrics   
    #########################################
    {
        "MetricID": "Pod memory reclaim latency",
        "Type": "latency",
        "Weight": 1.0,
    },
    #########################################
    # Pod Error Metrics   
    #########################################
    {
        "MetricID": "Pod OOM count",
        "Type": "error",
        "Weight": 0.5,
    },
    {
        "MetricID": "Pod memory fail count",
        "Type": "error",
        "Weight": 0.5,
    },
]