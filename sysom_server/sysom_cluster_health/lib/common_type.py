from dataclasses import dataclass
from enum import Enum
from typing import Optional

@dataclass
class Labels:
    cluster: str
    instance: Optional[str]
    namespace: Optional[str]
    pod: Optional[str]
    
    def __init__(self, cluster, instance=None, namespace=None, pod=None):
        self.cluster = cluster
        self.instance = instance
        self.namespace = namespace
        self.pod = pod

class Level(Enum):
    Cluster = "cluster"
    Node = "node"
    Pod = "pod"