import MultiGrafanaPannel from '../components/multiGrafanaPannel';
import { useIntl } from 'umi';

/**
 * Nginx应用观测
 * @returns 
 */
const AppObservableNginx = (props) => {
    const intl = useIntl();
    return (
        <MultiGrafanaPannel
            pannels={[
                {
                    "pannelId": "nginx_monitor",
                    "pannelName": intl.formatMessage({
                        id: 'pages.app_observable.monitor_dashboard',
                        defaultMessage: 'Monitor Dashboard',
                    }),
                    "pannelUrl": '/grafana/d/6Mztrm4Ik/nginx',
                },
                {
                    "pannelId": "nginx_event",
                    "pannelName": intl.formatMessage({
                        id: 'pages.app_observable.abnormal_events',
                        defaultMessage: 'Abnormal Events',
                    }),
                    "pannelUrl": '/grafana/d/HtuWUeSSz/nginx-event',
                }
            ]}
            {...props}
        />
    )
};
export default AppObservableNginx;