import {  useRef, useState, useEffect, useContext, useReducer } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import { delOSType, delKernelVersion, getOSTypeList, getKernelVersionList, submitOSType, submitKernelVersion } from '../../service';
import OSTypeKernelVersion from './KernelVersion'; 

const KernelVersionList = () => {
  const actionRef = useRef();
  const refNetListTable = useRef();
  const intl = useIntl();
  
  return (
    <PageContainer>
      <OSTypeKernelVersion />
    </PageContainer>
  );
};

export default KernelVersionList; 