import ProForm, { ProFormText, ProFormSelect, ProFormSwitch } from '@ant-design/pro-form';
import React, { useRef, useState, useEffect } from 'react';
import { Button } from 'antd';
import { useRequest, useIntl, FormattedMessage } from 'umi';
import ProCard from '@ant-design/pro-card';
import { postTask } from '../../diagnose/service'
import { submitKernelVersion, getOSTypeList } from '../service'

export default (props) => {
    const intl = useIntl();
    const formRef = useRef();
    const [readonlyone, setReadonlyOne] = useState(false);
    const [veroptionList,setVerOptionList] = useState([]);
    const [dataostype, setDataOsType] = useState([]);
    const { loading, error, run } = useRequest(submitKernelVersion, {
        manual: true,
        onSuccess: (result, params) => {
            formRef.current.resetFields();
            props?.onSuccess?.(result, params);
        },
    });
    const KernelConfigChange = (e) => {
        setReadonlyOne(e);
    }
    return (
        <ProCard>
            <ProForm
                onFinish={async (values) => {
                    run(values)
                }}
                formRef={formRef}
                submitter={{
                    submitButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                    resetButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                }}
                layout={"horizontal"}
                autoFocusFirstInput
            >
                <ProForm.Group>
                    <ProFormSelect
                        width="xs"
                        options={props.data === undefined ? [] : props.data}
                        name="os_type"
                        label={intl.formatMessage({id:'pages.hotfix.os_type', defaultMessage:'Operating System Name (Type)'})}
                        tooltip={intl.formatMessage({
                            id:'pages.hotfix.tooltips.select_os_type', 
                            defaultMessage:'This is the OS Type of this kernel verion. eg. Anolis for 4.19.91-26.an8.x86_64'
                        })}
                    />
                    <ProFormText
                        name="kernel_version"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.input_full_kernelversion',
                            defaultMessage: 'pages.hotfix.tooltips.input_full_kernelversion'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.kernel_version',
                            defaultMessage: 'Kernel version'
                        })}
                    />
                    <ProFormText
                        name="source"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.source_code_method',
                            defaultMessage: 'If you use git repo to manage your code, input the branch or tag of this kernel version; If you use .src.rpm, input the download link to the package'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.code_source',
                            defaultMessage: 'The source of your kernel source code'
                        })}
                    />
                    <ProFormText
                        name="devel_link"
                        width="md"
                        tooltip={intl.formatMessage({
                            id:'pages.hotfix.tooltips.devel_downloadlink',
                            defaultMessage: 'Please input the download link of the devel- package'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.devel_link',
                            defaultMessage: 'The link of kernel-devel package'
                        })}
                    />
                    <ProFormText
                        name="debuginfo_link"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.debuginfo_downloadlink',
                            defaultMessage: 'Please input the download link of the kernel-debuginfo package',
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.debuginfo_link',
                            defaultMessage: 'The download link of kernel-debuginfo package'
                        })}
                    />
                    <ProFormText
                        name="image"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.building_image',
                            defaultMessage: 'Please input the building image when building hotfix of this os'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.building_image',
                            defaultMessage: 'The building docker image use by this kernel version'
                        })}
                    />
                    <ProFormSwitch
                        style={{
                            marginBlockEnd: 16,
                        }}
                        name="use_src_package"
                        initialValue={readonlyone}
                        label={intl.formatMessage({
                            id:'pages.hotfix.use_src_rpm',
                            defaultMessage:'Use .src.rpm package'
                        })}
                        checked={readonlyone}
                        checkedChildren={intl.formatMessage({
                            id: 'pages.hotfix.yes',
                            defaultMessage: 'Yes'
                        })}
                        unCheckedChildren={intl.formatMessage({
                            id:'pages.hotfix.no',
                            defaultMessage: 'No'
                        })}
                        onChange={KernelConfigChange}
                    />
                    <Button type="primary" htmlType="submit" loading={loading}>{intl.formatMessage({
                        id: 'pages.hotfix.submit',
                        defaultMessage: 'Submit'
                    })}</Button>
                </ProForm.Group>
            </ProForm>
        </ProCard>
    )
}
