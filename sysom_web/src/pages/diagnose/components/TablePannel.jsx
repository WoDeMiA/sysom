import ProCard from '@ant-design/pro-card';
import ProTable from "@ant-design/pro-table";
import fieldModifier from "./fieldModifier"
import { Empty, Space } from 'antd';
 import { Typography } from 'antd';
const { Text } = Typography;
import { useIntl } from 'umi';
import { invokeTaskHook } from "../service"

const TablePannel = (props) => {
  const configs = props.configs
  const data = props.data
  const task_data = props.datas;
  const intl = useIntl();
  const refreshTask = props.refreshTask;


  //table background color render
  const bgColorRender = (elem, record) => {
    let text = elem;
    if (typeof (elem) === "object" && "props" in elem && "text" in elem.props && typeof (elem.props.text) === "string") {
      text = elem.props.text;
    }

    const [value, color] = fieldModifier(configs.fieldConfig, text, record, data)
    return {
      props: {
        style: {
          background: color,
          wordWrap: 'break-word',
          wordBreak: 'break-word'
        }
      },
      children: <div>{
        (typeof (elem) === "string") ?
          value :
          elem
      }</div>
    }
  }

  let columns = [];
  if (!!configs.columns) {
    columns = configs.columns;
    for (let i = 0; i < columns.length; i++) {
      let column = columns[i];
      if (!column.dataIndex) {
        column.dataIndex = column.key;
      }
      if (column.title.startsWith("pages.diagnose")) {
        column.title = intl.formatMessage({ id: column.title })
      }
      // column.render = bgColorRender;
      column.render = bgColorRender;
      if (column.valueType === "custom_options") {
        column.render = (elem, record) => {
          let operations = [];
          if (column.key in record && typeof (record[column.key]) === "object") {
            record[column.key].forEach((operation) => {
              let key = operation.key
              switch (operation.type) {
                case "LINK":
                  operations.push(<span key={key}>
                    {
                      <a onClick={() => {
                        window.open(operation.url)
                      }}>
                        {intl.formatMessage({
                          id: operation.label,
                          defaultMessage: operation.label
                        })}
                      </a>
                    }
                  </span>)
                  break
                case "DIAGNOSIS":
                  operations.push(<span key={key}>
                    {
                      <a onClick={() => {
                        message.warning("Not support DIAGNOSIS opt now!")
                      }}>
                        {intl.formatMessage({
                          id: operation.label,
                          defaultMessage: operation.label
                        })}
                      </a>
                    }
                  </span>)
                  break
                case "INVOKE_DIAGNOSIS_HOOK":
                  // {
                  //     "key":"ossre_xxx",
                  //     "label":"屏蔽告警",
                  //     "type":"INVOKE_DIAGNOSIS_HOOK",
                  //     "params":{
                  //         "xxx":"xxx"
                  //     }
                  // }
                  operations.push(<span key={key}>
                    {
                      <a onClick={() => {
                        invokeTaskHook({
                          task_id: task_data.task_id,
                          params: operation.params
                        })
                          .then(async (res) => {
                            if (refreshTask) {
                              await refreshTask(task_data)
                            }
                          })
                          .catch(err => {
                            console.log(err)
                          })
                      }}>
                        {intl.formatMessage({
                          id: operation.label,
                          defaultMessage: operation.label
                        })}
                      </a>
                    }
                  </span>)
                  break
              }
            })
            return <Space size={[10, 'small']} wrap>
              {
                operations
              }
            </Space>
          }
        }
      }
    }
  } else {
    if (!!data && data.length > 0) {
      let enableSortColumn = configs.tableConfig?.enableSortColumn;

      columns = data && Object.keys(data[0]).map((key) => {
        let res = {
          title: key,
          dataIndex: key,
          render: bgColorRender
        }

        if (!!enableSortColumn) {
          if (enableSortColumn.includes(key)) {
            res.sorter = (a, b) => a[key] - b[key]
          }
        }

        return res;
      });
      //filter reserve keyword
      const keyword = ["key", "children"]
      columns = columns?.filter((col) => !keyword.includes(col.title))
    }
  }

  return (
    <ProCard
      title={configs.title}
      tooltip={!!configs.tooltips ? configs.tooltips : ""}
      style={{ marginTop: 16, ...props.style }} bordered collapsible
      bodyStyle={{ padding: 0 }}
    >
      {
        data ?
          <ProTable
            options={false}
            dataSource={data}
            columns={columns}
            search={false}
            expandable={{
              rowExpandable: record => record.children?.length > 0,
            }}
            style={{ marginTop: 16 }}
            bordered
            col
            clapsible
            tableStyle={{
              whiteSpace: "pre-wrap"
            }}
          />
          : <Empty style={{ marginBottom: 20 }} image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={
              <div>Datasource  <Text type="danger"> {configs?.datasource} </Text> no data</div>
            } />

      }
    </ProCard>
  )
}

//    headerTitle={configs.title}

export default TablePannel
