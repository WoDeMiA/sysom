import { PageContainer } from '@ant-design/pro-layout';
import ProForm, { ProFormSelect, ProFormText, ProFormDigit } from '@ant-design/pro-form';
import ProCard from '@ant-design/pro-card';
import { message, Button, Row, Col } from 'antd';
import { useRequest, useIntl, FormattedMessage, request } from 'umi';
import { useState, useEffect, useRef } from 'react';
import { postConfig, getConfig } from '../service'
import styles from "../vmcore.less";
import ConfigTopContent from './ConfigTopContent'
import { data } from 'browserslist';

const { Divider } = ProCard;

const VmcoreConfig = () => {
  const [count, setCount] = useState(0)
  const formRef = useRef();
  const intl = useIntl();
  const { datalist } = useRequest(() => {
    return getVMcoreDetailList();
  });

  const getVMcoreDetailList = async () => {
    const msg = await request("/api/v1/vmcore/", {
      params: { get_config: 1 },
    });
    console.log(msg);
    if(msg.code === 200){
      setCount({ rawData: msg.data });
    }
  };

  const onListClick = async () => {
    // console.log("3333333333333",formRef.current.getFieldValue(['server_host']),formRef.current.getFieldValue(['mount_point']));
    const msg = await request("/api/v1/vmcore/vmcore_config_test/", {
      params: { 
        server_host: formRef.current.getFieldValue(['server_host']),
        mount_point: formRef.current.getFieldValue(['mount_point'])
      },
    });
    if(msg.code === 200){
      //formRef.current.resetFields('');
      message.success('连接测试成功！');
    }else{
      message.error('连接测试失败！');
    }
  }
  
  const { loading, error, run } = useRequest(postConfig, {
    manual: true,
    onSuccess: (result, params) => {
      // params.push({post_config:1});
      console.log(result, params,"121");
      formRef.current.resetFields('');
      getVMcoreDetailList();
    },
  });
  console.log(count);
  const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }      
  return (
    <PageContainer>
      <ProCard title={intl.formatMessage({
        id: 'pages.vmcore.configuration',
        defaultMessage: 'Current configuration:',
      })}>
        {count.rawData && <ConfigTopContent data={count.rawData} />}

        <Divider className={styles.dividerpoint}/>
        
        <ProForm
          {...formItemLayout}
          onFinish={async (values) => {
            run(values)
          }}
          submitter={{
            submitButtonProps: {
              style: {
                display: 'none',
              },
            },
            resetButtonProps: {
              style: {
                display: 'none',
              },
            },
          }}
          formRef={formRef}
          layout={"horizontal"}
          autoFocusFirstInput
        >
          <ProFormText
            label="post_config"
            width="md"
            name="post_config"
            initialValue={1}
            hidden
          />

          <ProFormText
            label={intl.formatMessage({
              id: 'pages.vmcore.warehousename',
              defaultMessage: 'Warehouse name',
            })}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage
                    id="pages.configform.warehouse_required"
                    defaultMessage="warehouse is required"
                  />
                ),
              },
            ]}
            width="md"
            name="name"
            placeholder={<FormattedMessage
              id="pages.vmcore.warehousevmcore"
              defaultMessage="Name of the warehouse where the vmcore is faulty"
            />}
          />

          <ProFormText
            label={intl.formatMessage({
              id: 'pages.vmcore.NFSserverIP',
              defaultMessage: 'NFS server IP address',
            })}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage
                    id="pages.configform.NFSserverIP_required"
                    defaultMessage="NFS Server IP Address is required"
                  />
                ),
              },
            ]}
            width="md"
            name="server_host"
            placeholder={<FormattedMessage
              id="pages.vmcore.IPaddress"
              defaultMessage="IP address of the NFS server, for example, 192.168.0.1"
            />}
          />

          <ProFormText
            label={intl.formatMessage({
              id: 'pages.vmcore.mountdirectory',
              defaultMessage: 'Mount directory',
            })}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage
                    id="pages.configform.mount_required"
                    defaultMessage="Mount the directory is required"
                  />
                ),
              },
            ]}
            width="md"
            name="mount_point"
            placeholder={<FormattedMessage
              id="pages.vmcore.NFSserver"
              defaultMessage="The NFS server shares the directory"
            />}
          />
          <ProFormText
            label={intl.formatMessage({
              id: 'pages.vmcore.storageduration',
              defaultMessage: 'Storage duration',
            })}
            width="md"
            name="days"
            placeholder={<FormattedMessage
              id="pages.vmcore.vmcorestorage"
              defaultMessage="vmcore storage duration (unit: day)"
            />}
          />
          
          <Button type="primary" className={styles.configleft17} htmlType="button" onClick={() => onListClick()}><FormattedMessage id="pages.vmcore.testconnector" defaultMessage="Test connector" /></Button>
          <Button type="primary" className={styles.ml20} htmlType="submit" loading={loading}><FormattedMessage id="pages.vmcore.saveconfiguration" defaultMessage="Save configuration" /></Button>
        </ProForm>
      </ProCard>
    </PageContainer>
  );
};

export default VmcoreConfig;
