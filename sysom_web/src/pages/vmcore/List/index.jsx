import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import VmcoreTableList from '../components/VmcoreTableList';
import VmcoreCard from '../components/VmcoreCard';
import { getVmcore } from "../service";
import { useIntl, FormattedMessage } from 'umi';

const { Divider } = ProCard;

const VmcoreList = () => {
  const intl = useIntl();
  return (
    <PageContainer>
      <VmcoreCard />
      <Divider />
      <VmcoreTableList headerTitle={intl.formatMessage({
        id: 'pages.vmcore.list',
        defaultMessage: 'Outage list',
      })} search={true} request={getVmcore} />
    </PageContainer>
  );
};

export default VmcoreList;
