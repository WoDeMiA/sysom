/*
 * @Author: wb-msm241621
 * @Date: 2024-03-11 17:40:55
 * @LastEditTime: 2024-03-14 17:24:51
 * @Description: 
 */
/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
    // return false to prevent the error from failing the test if it matches
    // the specific error message
    if (err.message.includes('ResizeObserver loop completed with undelivered notifications')) {
      return false;
    }
    // else let Cypress handle the exception as it normally does
    return true;
});

describe("SysOM Diagnosis Test -- Query", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke diagnose query, and check result", () => {
        cy.intercept("GET", "/api/v1/tasks/?*").as("getDiagnoseTaskList")

        // 1. 访问诊断查询页面
        cy.visit("diagnose/query")

        cy.wait("@getDiagnoseTaskList", { timeout: 10000 }).then((interception) => {
            expect(interception).to.have.property('response')
            expect(interception.response?.body.code, 'code').to.equal(200)
            expect(interception.response.statusCode).to.equal(200)

            // cy.get('.ant-table-tbody').find('tr').should("have.length.gte", 0)
            cy.wait(1000)
            cy.get('.ant-table-content').find('table').then(($el) => {
                if ($el.text().includes("No data")) {
                    cy.wrap($el).contains("No data")
                } else {
                    // 断言告警列表数据内容大于等于1
                    cy.wrap($el).find('.ant-table-tbody').find('tr').should('have.length.gte', 1)
                    // 断言表头字段数量是否等于5
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').should('have.length.gte', 5)

                    // 断言诊断ID是否为UUID
                    cy.wrap($el).find(".ant-table-tbody").find('tr').eq(0).find('td').eq(1).invoke("text").then((text) => {
                        expect(text).to.match(/[a-zA-Z0-9]{8}/);
                    })

                    // 断言诊断时间格式
                    cy.wrap($el).find(".ant-table-tbody").find('tr').eq(0).find('td').eq(2).invoke("text").then((text) => {
                        const data = new Date(text);
                        expect(data.toString()).not.to.equal('Invalid Date!')
                    })

                    // 断言诊断状态是否在枚举类型中
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(3).invoke("text").then((text) => {
                        expect(text.trim()).to.be.oneOf(["诊断完毕", "异常", "准备中", "运行中"])
                    })

                    // 断言诊断参数是否大于1
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(4).find("span").should('have.length.gt', 1)
                }
            })
        })

        cy.wait(2000)

        // 2. 诊断查询参数悬着ssh通道
        cy.get('#channel').click({force: true})
        cy.get(".rc-virtual-list-holder-inner").contains("SSH通道").click()

        cy.get(':nth-child(2) > .ant-btn').click()
        cy.wait(2000)

        // 3. 
        cy.get(".ant-table-content").find("table").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find(".ant-table-tbody").find('tr').eq(0).find('td').eq(5).find('.ant-space-item').eq(0).then(($el) => {
                    if ($el.text().includes("查看出错信息")) {
                        // 诊断失败后查看错误信息
                        cy.wrap($el).get("a").contains("查看出错信息").click()
                        cy.wait(1000)
                    } else {
                        // 诊断成功后查看诊断结果
                        cy.wrap($el).get("a").contains("查看诊断结果").click()
                        cy.wait(1000)

                        cy.get(".ant-pro-card-header").eq(0).scrollIntoView()
                        cy.get(".ant-pro-card-header").should("contain.text", "诊断结果")
                    }
                })
            }
        })
        cy.wait(1500)
        cy.scrollTo("top")

        cy.get('.ant-pro-card-body > .ant-form > .ant-space-align-center > :nth-child(1) > .ant-btn').click()
    })
})
