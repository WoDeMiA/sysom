/*
 * @Author: wb-msm241621
 * @Date: 2023-12-20 17:15:53
 * @LastEditTime: 2023-12-22 15:54:55
 * @Description: 
 */
/// <reference types="cypress" />


describe("SysOM dashboard Monitor Dashboard Test", () => {
    beforeEach(() => {
        cy.login();
    })

    it("Dashboard monitor test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)

        /* Quick CPU / Mem / Disk */
        // 当前面板 CPU Busy
        cy.panelNumericalValueGteTest("CPU Busy")

        // 当前面板Sys Load (5m avg)
        cy.panelNumericalValueGteTest("Sys Load (5m avg)")

        // 当前面板Sys Load (15m avg)
        cy.panelNumericalValueGteTest("Sys Load (15m avg)")
        
        // 当前面板RAM Used
        cy.panelNumericalValueGteTest("RAM Used")

        // 当前面板SWAP Used
        cy.panelNumericalValueGteTest("SWAP Used")

        // 当前面板Root FS Used
        cy.panelNumericalValueGteTest("Root FS Used")

        // 当前面板 CPU Cores
        cy.panelNumericalValueGteTest("CPU Cores")

        // 当前面板 Uptime
        cy.panelNumericalValueGteTest("Uptime")

        // 当前面板 RootFS Total
        cy.panelNumericalValueGteTest("RootFS Total")

        // 当前面板 RAM Total
        cy.panelNumericalValueGteTest("RAM Total")

        // 当前面板 SWAP Total
        cy.panelNumericalValueGteTest("SWAP Total")
    })

    it("Basic CPU / Mem / Net / Disk Test", () => {
        /*
         * Basic CPU / Mem / Net / Disk
         */

        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 当前面板 CPU Basic
        cy.panelAtagValueGteTest("CPU Basic")

        // 当前面板Memory Basic
        cy.panelAtagValueGteTest("Memory Basic")
        
        // 当前面板 Network Traffic Basic
        cy.panelAtagValueGteTest("Network Traffic Basic")
        
        // 当前面板 Disk Space Used Basic
        cy.panelAtagValueGteTest("Disk Space Used Basic")
    })

    it("CPU / Memory / Net / Disk Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开CPU / Memory / Net / Disk 标签
        cy.openMainLabel("CPU / Memory / Net / Disk")

        // 当前面板 CPU
        const cpuPropertys = [
            // 'System - Processes executing in kernel mode',
            // 'User - Normal processes executing in user mode',
            // 'Nice - Niced processes executing in user mode',
            // 'Idle - Waiting for something to happen',
            // 'Iowait - Waiting for I/O to complete',
            // 'Irq - Servicing interrupts',
            // 'Softirq - Servicing softirqs',
            // 'Steal - Time spent in other operating systems when running in a virtualized environment'
        ]
        cy.panelFoldLineTableGteTest('CPU')
        
        // 当前面板 Memory Stack
        // const memoryStackPropertys = [
        //     'Apps - Memory used by user-space applications',
        //     'PageTables - Memory used to map between virtual and physical memory addresses',
        //     'SwapCache - Memory that keeps track of pages that have been fetched from swap but not yet been modified',
        //     'Slab - Memory used by the kernel to cache data structures for its own use (caches like inode, dentry, etc)',
        //     'Cache - Parked file data (file content) cache',
        //     'Buffers - Block device (e.g. harddisk) cache',
        //     'Unused - Free memory unassigned',
        //     'Swap - Swap space used',
        //     'Hardware Corrupted - Amount of RAM that the kernel identified as corrupted / not working',
        // ]
        const memoryStackPropertys = []
        cy.panelFoldLineTableGteTest('Memory Stack', memoryStackPropertys)
        
        // 当前面板 Network Traffic
        const networkTrafficPropertys = [
            // 'eth0 - Receive',
            // 'lo - Receive',
            // 'eth0 - Transmit',
            // 'lo - Transmit',
        ]
        cy.panelFoldLineTableGteTest('Network Traffic', networkTrafficPropertys)

        // 当前面板 Disk Space Used
        const diskSpaceUsedPropertys = [
            // '/run/user/0',
            // '/run',
            // '/usr/local/sysom/server/builder/hotfix',
            // '/',
        ]
        cy.panelFoldLineTableGteTest('Disk Space Used', diskSpaceUsedPropertys)

        // 当前面板 Disk IOps
        const diskIopsPropertys = [
            // 'vda - Writes completed'
        ]
        cy.panelFoldLineTableGteTest('Disk IOps', diskIopsPropertys)

        // 当前面板 I/O Usage Read / Write
        const ioUsageReadWritePropertys = [
            // 'vda - Successfully read bytes',
            // 'vda - Successfully written bytes'
        ]
        cy.panelFoldLineTableGteTest('I/O Usage Read / Write', ioUsageReadWritePropertys)

        // 当前面板 I/O Utilization
        const ioUtilizationPropertys = [
            // 'vda'
        ]
        cy.panelFoldLineTableGteTest('I/O Utilization', ioUtilizationPropertys)


    })

    it("Memory Meminfo Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Memory Meminfo 标签
        cy.openMainLabel("Memory Meminfo")

        // 当前面板 Memory Active / Inactive
        const memoryActivePropertys = [
            // 'Inactive - Memory which has been less recently used.  It is more eligible to be reclaimed for other purposes',
            // 'Active - Memory that has been used more recently and usually not reclaimed unless absolutely necessary'
        ]
        cy.panelFoldLineTableGteTest('Memory Active / Inactive', memoryActivePropertys)
        
        // 当前面板 Memory Commited
        const memoryCommitedPropertys = [
            // 'Committed_AS - Amount of memory presently allocated on the system',
            // 'CommitLimit - Amount of memory currently available to be allocated on the system'
        ]
        cy.panelFoldLineTableGteTest('Memory Commited', memoryCommitedPropertys)

        // 当前面板 Memory Active / Inactive Detail
        const memoryActiveDetailPropertys = [
            // 'Inactive_file - File-backed memory on inactive LRU list',
            // 'Inactive_anon - Anonymous and swap cache on inactive LRU list, including tmpfs (shmem)',
            // 'Active_file - File-backed memory on active LRU list',
            // 'Active_anon - Anonymous and swap cache on active least-recently-used (LRU) list, including tmpfs'
        ]
        cy.panelFoldLineTableGteTest('Memory Active / Inactive Detail', memoryActiveDetailPropertys)

        // 当前面板 Memory Writeback and Dirty
        const memoryWritebackAndDirtyPropertys = [
            // 'Writeback - Memory which is actively being written back to disk',
            // 'WritebackTmp - Memory used by FUSE for temporary writeback buffers',
            // 'Dirty - Memory which is waiting to get written back to the disk'
        ]
        cy.panelFoldLineTableGteTest('Memory Writeback and Dirty', memoryWritebackAndDirtyPropertys)

        // 当前面板 Memory Shared and Mapped
        const memorySharedAndMappedPropertys = [
            // 'Mapped - Used memory in mapped pages files which have been mmaped, such as libraries',
            // 'Shmem - Used shared memory (shared between several processes, thus including RAM disks)',
            // 'ShmemHugePages - Memory used by shared memory (shmem) and tmpfs allocated with huge pages',
            // 'ShmemPmdMapped - Ammount of shared (shmem/tmpfs) memory backed by huge pages'
        ]
        cy.panelFoldLineTableGteTest('Memory Shared and Mapped', memorySharedAndMappedPropertys)

        // 当前面板 Memory Slab
        const memorySlabPropertys = [
            // 'SUnreclaim - Part of Slab, that cannot be reclaimed on memory pressure',
            // 'SReclaimable - Part of Slab, that might be reclaimed, such as caches'
        ]
        cy.panelFoldLineTableGteTest('Memory Slab', memorySlabPropertys)

        // 当前面板 Memory Vmalloc
        const memoryVmallocPropertys = [
            // "VmallocChunk - Largest contigious block of vmalloc area which is free",
            // "VmallocTotal - Total size of vmalloc memory area",
            // "VmallocUsed - Amount of vmalloc area which is used"
        ]
        cy.panelFoldLineTableGteTest('Memory Vmalloc', memoryVmallocPropertys)

        // 当前面板 Memory Bounce
        const memoryBouncePropertys = [
            // 'Bounce - Memory used for block device bounce buffers'
        ]
        cy.panelFoldLineTableGteTest('Memory Bounce', memoryBouncePropertys)

        // 当前面板 Memory Anonymous
        const memoryAnonymousPropertys = [
            // 'AnonHugePages - Memory in anonymous huge pages',
            // 'AnonPages - Memory in user pages not backed by files'
        ]
        cy.panelFoldLineTableGteTest('Memory Anonymous', memoryAnonymousPropertys)

        // 当前面板 Memory Kernel / CPU
        const memoryKernelCPUPropertys = [
            // 'KernelStack - Kernel memory stack. This is not reclaimable',
            // 'PerCPU - Per CPU memory allocated dynamically by loadable modules'
        ]
        cy.panelFoldLineTableGteTest('Memory Kernel / CPU', memoryKernelCPUPropertys)

        // 当前面板 Memory HugePages Counter
        const MemoryHugePagesCounterpropertys = [
        //     'HugePages_Free - Huge pages in the pool that are not yet allocated',
        //     'HugePages_Rsvd - Huge pages for which a commitment to allocate from the pool has been made, but no allocation has yet been made',
        //     'HugePages_Surp - Huge pages in the pool above the value in /proc/sys/vm/nr_hugepages'
        ]
        cy.panelFoldLineTableGteTest('Memory HugePages Counter', MemoryHugePagesCounterpropertys)
    
        // 当前面板 Memory HugePages Size
        const MemoryHugePagesSizePropertys = [
            // 'HugePages - Total size of the pool of huge pages',
            // 'Hugepagesize - Huge Page size'
        ]
        cy.panelFoldLineTableGteTest('Memory HugePages Size', MemoryHugePagesSizePropertys)

        // 当前面板 Memory DirectMap
        const MemoryDirectMapPropertys = [
            // 'DirectMap1G - Amount of pages mapped as this size',
            // 'DirectMap2M - Amount of pages mapped as this size',
            // 'DirectMap4K - Amount of pages mapped as this size'
        ]
        cy.panelFoldLineTableGteTest('Memory DirectMap', MemoryDirectMapPropertys)

        // 当前面板 Memory Unevictable and MLocked
        const MemoryUnevictableandMLockedPropertys = [
            // "Unevictable - Amount of unevictable memory that can't be swapped out for a variety of reasons",
            // "MLocked - Size of pages locked to memory using the mlock() system call"
        ]
        cy.panelFoldLineTableGteTest('Memory Unevictable and MLocked', MemoryUnevictableandMLockedPropertys)

        // 当前面板 Memory NFS
        const MemoryNFSPropertys = [
            // "NFS Unstable - Memory in NFS pages sent to the server, but not yet commited to the storage"
        ]
        cy.panelFoldLineTableGteTest('Memory NFS')
    })

    it("Memory Vmstat Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Memory Vmstat 标签
        cy.openMainLabel("Memory Vmstat")

        // 当前面板 Memory Pages In / Out
        cy.panelFoldLineTableGteTest('Memory Pages In / Out')

        // 当前面板 Memory Pages Swap In / Out
        cy.panelFoldLineTableGteTest('Memory Pages Swap In / Out')

        // 当前面板 Memory Page Faults
        cy.panelFoldLineTableGteTest('Memory Page Faults')

        // 当前面板OOM Killer
        cy.panelFoldLineTableGteTest('OOM Killer')
    })

    it("System Timesync Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 System Timesync 标签
        cy.openMainLabel("System Timesync")

        // 当前面板 Time Syncronized Drift
        cy.panelFoldLineTableGteTest('Time Syncronized Drift')

        // 当前面板 Time PLL Adjust
        cy.panelFoldLineTableGteTest('Time PLL Adjust')
        
        // 当前面板 Time Syncronized Status
        cy.panelFoldLineTableGteTest('Time Syncronized Status')
        
        // 当前面板 Time Misc
        cy.panelFoldLineTableGteTest('Time Misc')
    })

    it("System Processes Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开  标签
        cy.openMainLabel("System Processes")
        
        // 当前面板 Processes Status
        cy.panelFoldLineTableGteTest('Processes Status')
        
        // 当前面板 Processes State
        cy.panelNoDataTest('Processes State')

        // 当前面板 Processes  Forks
        cy.panelFoldLineTableGteTest('Processes  Forks')

        // 当前面板 Processes Memory
        cy.panelFoldLineTableGteTest('Processes Memory')

        // 当前面板 PIDs Number and Limit
        cy.panelNoDataTest('PIDs Number and Limit')
        // cy.getPannelContentByTitle("PIDs Number and Limit").find("tbody tr").should("have.length.gte", 1)

        // 当前面板 Process schedule stats Running / Waiting
        cy.panelFoldLineTableGteTest('Process schedule stats Running / Waiting')

        // 当前面板 Threads Number and Limit
        cy.panelNoDataTest('Threads Number and Limit')
    })

    it("System Misc Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 System Misc 标签
        cy.openMainLabel("System Misc")

        // 当前面板 Context Switches / Interrupts
        cy.panelFoldLineTableGteTest("Context Switches / Interrupts")

        // 当前面板 System Load
        cy.panelFoldLineTableGteTest("System Load")

        // 当前面板 Interrupts Detail
        cy.panelNoDataTest("Interrupts Detail")

        // 当前面板 Schedule timeslices executed by each cpu
        cy.panelFoldLineTableGteTest("Schedule timeslices executed by each cpu")

        // 当前面板 Entropy
        cy.panelFoldLineTableGteTest("Entropy")

        // 当前面板 CPU time spent in user and system contexts
        cy.panelFoldLineTableGteTest("CPU time spent in user and system contexts")

        // 当前面板 File Descriptors
        cy.panelFoldLineTableGteTest("File Descriptors")
    })

    it("Hardware Misc Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Hardware Misc 标签
        cy.openMainLabel("Hardware Misc")

        // 当前面板 Hardware temperature monitor
        cy.panelNoDataTest("Hardware temperature monitor")

        // 当前面板 Throttle cooling device
        cy.panelFoldLineTableGteTest("Throttle cooling device")

        // 当前面板 Power supply
        cy.panelNoDataTest("Power supply")
    })

    it("Systemd Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Systemd 标签
        cy.openMainLabel("Systemd")

        // 当前面板 Systemd Sockets
        cy.panelNoDataTest("Systemd Sockets")

        // 当前面板 Systemd Units State
        cy.panelNoDataTest("Systemd Units State")
    })

    it("Storage Disk Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Storage Disk 标签
        cy.openMainLabel("Storage Disk")

        // 当前面板 Disk IOps Completed
        cy.panelFoldLineTableGteTest("Disk IOps Completed")
        
        // 当前面板 Disk R/W Data
        cy.panelFoldLineTableGteTest("Disk R/W Data")

        // 当前面板 Disk Average Wait Time
        cy.panelFoldLineTableGteTest("Disk Average Wait Time")

        // 当前面板 Average Queue Size
        cy.panelFoldLineTableGteTest("Average Queue Size")

        // 当前面板 Disk R/W Merged
        cy.panelFoldLineTableGteTest("Disk R/W Merged")

        // 当前面板 Time Spent Doing I/Os
        cy.panelFoldLineTableGteTest("Time Spent Doing I/Os")

        // 当前面板 Instantaneous Queue Size
        cy.getPannelContentByTitle("Instantaneous Queue Size").find("canvas").should("have.length.gte", 1)
        cy.getPannelContentByTitle("Instantaneous Queue Size").find("tbody").should("have.length.gte", 1)

        // 当前面板 Disk IOps Discards completed / merged
        cy.getPannelContentByTitle("Disk IOps Discards completed / merged").find("canvas").should("have.length.gte", 1)
        cy.getPannelContentByTitle("Disk IOps Discards completed / merged").find("tbody").should("have.length.gte", 1)
    })

    it("Storage Filesystem Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Storage Filesystem 标签
        
        cy.openMainLabel("Storage Filesystem")

        // 当前面板 Filesystem space available
        cy.panelFoldLineTableGteTest("Filesystem space available")

        // 当前面板 File Nodes Free
        cy.panelFoldLineTableGteTest("File Nodes Free")

        // 当前面板 File Descriptor
        cy.panelFoldLineTableGteTest("File Descriptor")

        // 当前面板 File Nodes Size
        cy.panelFoldLineTableGteTest("File Nodes Size")

        // 当前面板 Filesystem in ReadOnly / Error
        // cy.panelFoldLineTableGteTest("Filesystem in ReadOnly / Error")
        cy.getPannelContentByTitle("Filesystem in ReadOnly / Error").find("canvas").should('be.empty')
        cy.getPannelContentByTitle("Filesystem in ReadOnly / Error").find('tbody').should('be.empty')
    })

    it("Network Traffic Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Network Traffic 标签
        cy.openMainLabel("Network Traffic")

        // 当前面板 Network Traffic by Packets
        cy.panelFoldLineTableGteTest("Network Traffic by Packets")

        // 当前面板 Network Traffic Errors
        cy.panelFoldLineTableGteTest("Network Traffic Errors")

        // 当前面板 Network Traffic Drop
        cy.panelFoldLineTableGteTest("Network Traffic Drop")

        // 当前面板 Network Traffic Compressed
        cy.panelFoldLineTableGteTest("Network Traffic Compressed")

        // 当前面板 Network Traffic Multicast
        cy.panelFoldLineTableGteTest("Network Traffic Multicast")

        // 当前面板 Network Traffic Fifo
        cy.panelFoldLineTableGteTest("Network Traffic Fifo")

        // 当前面板 Network Traffic Frame
        cy.panelFoldLineTableGteTest("Network Traffic Frame")

        // 当前面板 Network Traffic Carrier
        cy.panelFoldLineTableGteTest("Network Traffic Carrier")

        // 当前面板 Network Traffic Colls
        cy.panelFoldLineTableGteTest("Network Traffic Colls")

        // 当前面板 NF Contrack
        cy.panelNoDataTest("NF Contrack")
        cy.wait(4000)

        // 当前面板 ARP Entries
        cy.panelFoldLineTableGteTest("ARP Entries")

        // 当前面板 MTU
        cy.panelFoldLineTableGteTest("MTU")

        // 当前面板 Speed
        cy.panelFoldLineTableGteTest("Speed")

        // 当前面板 Queue Length
        cy.panelFoldLineTableGteTest("Queue Length")

        // 当前面板 Softnet Packets
        cy.panelFoldLineTableGteTest("Softnet Packets")

        // 当前面板 Softnet Out of Quota
        cy.panelFoldLineTableGteTest("Softnet Out of Quota")

        // 当前面板 Network Operational Status
        cy.panelFoldLineTableGteTest("Network Operational Status")

    })

    it("Network Sockstat Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Network Sockstat 标签
        cy.openMainLabel("Network Sockstat")

        // 当前面板 Sockstat TCP
        cy.panelFoldLineTableGteTest("Sockstat TCP")

        // 当前面板 Sockstat UDP
        cy.panelFoldLineTableGteTest("Sockstat UDP")
        
        // 当前面板 Sockstat Used
        cy.panelFoldLineTableGteTest("Sockstat Used")

        // 当前面板 Sockstat Memory Size
        cy.panelFoldLineTableGteTest("Sockstat Memory Size")

        // 当前面板 Sockstat FRAG / RAW
        cy.panelFoldLineTableGteTest("Sockstat FRAG / RAW")
    })

    it("Network Netstat Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Network Netstat 标签
        cy.openMainLabel("Network Netstat")

        // 当前面板 Netstat IP In / Out Octets
        cy.panelFoldLineTableGteTest("Netstat IP In / Out Octets")
        
        // 当前面板 Netstat IP Forwarding
        cy.panelFoldLineTableGteTest("Netstat IP Forwarding")

        // 当前面板 ICMP In / Out
        cy.panelFoldLineTableGteTest("ICMP In / Out")

        // 当前面板 ICMP Errors
        cy.panelFoldLineTableGteTest("ICMP Errors")

        // 当前面板 UDP In / Out
        cy.panelFoldLineTableGteTest("UDP In / Out")

        // 当前面板 UDP Errors
        cy.panelFoldLineTableGteTest("UDP Errors")

        // 当前面板 TCP In / Out
        cy.panelFoldLineTableGteTest("TCP In / Out")

        // 当前面板 TCP Errors
        cy.panelFoldLineTableGteTest("TCP Errors")

        // 当前面板 TCP Connections
        cy.panelFoldLineTableGteTest("TCP Connections")

        // 当前面板 TCP SynCookie
        cy.panelFoldLineTableGteTest("TCP SynCookie")

        // 当前面板 TCP Direct Transition
        cy.panelFoldLineTableGteTest("TCP Direct Transition")
    })

    it("Node Exporter Test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/dashboard");

        // 2. 等待页面加载完成
        cy.wait(2000)
        
        // 3. 关闭Quick CPU / Mem / Disk标签
        cy.openMainLabel("Quick CPU / Mem / Disk")

        // 4. 打开 Node Exporter 标签
        cy.openMainLabel("Node Exporter")

        // 当前面板 Node Exporter Scrape Time
        cy.panelFoldLineTableGteTest("Node Exporter Scrape Time")

        // 当前面板 Node Exporter Scrape
        cy.panelFoldLineTableGteTest("Node Exporter Scrape")
    })
})
