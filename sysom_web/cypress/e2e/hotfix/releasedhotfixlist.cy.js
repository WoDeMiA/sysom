/* ==== Test Created with Cypress Studio ==== */
/**
 * This js is use for automaticly test the function
 * of released hotfix list.
 */

it('releasedHotfixList', function() {
  cy.login()
  /** Test for the formal hotfix list */
  cy.visit('/hotfix/released');
  Cypress.on('uncaught:exception', (err, runnable) => {
      if (err.message.includes('ResizeObserver loop completed with undelivered notifications')) {
        return false;
      }
    });
  cy.get('.ant-pro-table-list-toolbar-right > :nth-child(1) > .ant-space > [style=""] > .ant-btn').click();
  /* ==== Create one record ==== */
  cy.get('.ant-modal-body > .ant-form > :nth-child(2) > :nth-child(1) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #hotfix_id').clear('1');
  cy.get('.ant-modal-body > .ant-form > :nth-child(2) > :nth-child(1) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #hotfix_id').type('123456');
  cy.get('.ant-modal-body > .ant-form > :nth-child(2) > :nth-child(2) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #released_kernel_version').clear('4');
  cy.get('.ant-modal-body > .ant-form > :nth-child(2) > :nth-child(2) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #released_kernel_version').type('4.19.91-26.al7.x86_64');
  cy.get(':nth-child(1) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-select > .ant-select-selector > .ant-select-selection-search > #serious').click();
  cy.get('.ant-select-item-option-active > .ant-select-item-option-content').click();
  cy.get('.ant-row-space-around > :nth-child(2) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-select > .ant-select-selector > .ant-select-selection-search > #fix_system').click();
  cy.get(':nth-child(7) > :nth-child(1) > .ant-select-dropdown > :nth-child(1) > .rc-virtual-list > .rc-virtual-list-holder > :nth-child(1) > .rc-virtual-list-holder-inner > .ant-select-item-option-active').click();
  cy.get('#serious_explain').click();
  cy.get('#download_link').clear('h');
  cy.get('#download_link').type('https://download.com');
  cy.get(':nth-child(5) > :nth-child(2) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-picker > .ant-picker-input > #released_time').click();
  cy.get('.ant-picker-now-btn').click();
  cy.get('#description').click();
  cy.get('[style="display: flex; justify-content: flex-end;"] > .ant-space > :nth-child(2) > .ant-btn > span').click();
  /* ==== End of create one record ==== */

  /* ==== Test Edit one record ==== */
  cy.visit('http://119.23.44.4/hotfix/released');
  cy.get('.ant-table-cell-fix-right > .ant-btn').click({multiple: true});
  cy.get(':nth-child(5) > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > #serious_explain').click().type('aaaaaa');
  cy.get(':nth-child(6) > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > #description').click().type('bbbbb');
  cy.get(':nth-child(7) > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-select > .ant-select-selector > .ant-select-selection-item').click();
  cy.get('.ant-select-item-option-active').click();
  cy.get('.ant-drawer-footer > [style="display: flex; justify-content: flex-end;"] > .ant-space > :nth-child(2) > .ant-btn > span').click();
  /* ==== End of Test Edit one record ==== */

  /** Test with upload file */
  cy.visit('http://119.23.44.4/hotfix/released');
  cy.get('.ant-pro-table-list-toolbar-right > :nth-child(1) > .ant-space > :nth-child(2) > .ant-btn').click();
  //cy.get('.ant-upload-drag-container input[type="file"]').click()
  // 获取文件 fixture
  cy.fixture('Test.xlsx', 'binary').then(fileContent => {
	const blob = Cypress.Blob.binaryStringToBlob(fileContent);
	// 获取上传组件
	cy.get('.ant-upload-drag-container').then($element => {
	  // 创建一个包含文件信息的拖放事件
	  const dragEvent = {
		dataTransfer: {
		  files: [
			// 你可以自定义这个 File 实例，确保文件类型和大小与你的测试文件匹配
			new File([blob], 'Test.xlsx'),
		  ],
		},
	  };
  
	  // 触发 'dragenter' 和 'dragover' 事件来模拟拖入文件的动作
	  cy.wrap($element).trigger('dragenter', dragEvent);
	  cy.wrap($element).trigger('dragover', dragEvent);
  
	  // 触发 'drop' 事件来模拟文件放下的动作，从而触发文件上传
	  cy.wrap($element).trigger('drop', dragEvent);
	});
  });;
  /** Test for the released hotfix record filter */
  cy.get('.ant-modal-footer > [style="display: flex; justify-content: flex-end;"] > .ant-space > :nth-child(2) > .ant-btn').click();
  cy.get('.ant-row-start > :nth-child(1) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #hotfix_id').clear('1');
  cy.get('.ant-row-start > :nth-child(1) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #hotfix_id').type('12343');
  cy.get('.ant-form-item-control-input-content > [style="gap: 16px;"] > :nth-child(1) > .ant-space > :nth-child(2) > .ant-btn > span').click();
  cy.get('.ant-form-item-control-input-content > [style="gap: 16px;"] > :nth-child(1) > .ant-space > :nth-child(1) > .ant-btn > span').click();
  cy.get(':nth-child(2) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #released_kernel_version').clear('4');
  cy.get(':nth-child(2) > .ant-form-item > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-input-affix-wrapper > #released_kernel_version').type('4.19');
  cy.get('.ant-form-item-control-input-content > [style="gap: 16px;"] > :nth-child(1) > .ant-space > :nth-child(2) > .ant-btn').click();
  cy.get('.ant-pro-form-collapse-button').click();
  cy.get('#released_kernel_version').clear();
  cy.get('#released_kernel_version').type('4.19.91-26.al7.x86_64');
  cy.get('.ant-form-item-control-input-content > [style="gap: 16px;"] > :nth-child(1) > .ant-space > :nth-child(2) > .ant-btn > span').click();

})